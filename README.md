# Platform



## Getting started

#### Learn the Docker Basics

1. [ ] 💻 [Docker Crash Course](https://blog.gruntwork.io/a-crash-course-on-docker-34073b9e1833)

After you have completed the guided trainings above, you should be able to answer the following questions:

1. [ ] What kind of benefits do we get form using containers?
1. [ ] Is Docker virtualizing the kernel or userspace?
1. [ ] Will changes made within a running container persist across restarts?

#### Learn the Kubernetes Basics

1. [ ] 💻 [Kubernetes Crash Course](https://blog.gruntwork.io/a-crash-course-on-kubernetes-a96c3891ad82)
1. [ ] 💻 [Create a local Cluster with kind](https://kind.sigs.k8s.io/docs/user/quick-start/)
1. [ ] 💻 [Deploy an App](https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-app/)
1. [ ] 💻 [Explore Your App](https://kubernetes.io/docs/tutorials/kubernetes-basics/explore/)
1. [ ] 💻 [Expose Your App](https://kubernetes.io/docs/tutorials/kubernetes-basics/expose/)
1. [ ] 💻 [Scale Your App](https://kubernetes.io/docs/tutorials/kubernetes-basics/scale/)
1. [ ] 💻 [Update Your App](https://kubernetes.io/docs/tutorials/kubernetes-basics/update/)
1. [ ] 💻 [Configure Redis using a ConfigMap](https://kubernetes.io/docs/tutorials/configuration/configure-redis-using-configmap/)
1. [ ] 💻 [Deploy the Guestbook App](https://kubernetes.io/docs/tutorials/stateless-application/guestbook/)
1. [ ] 💻 [Managing Secrets](https://kubernetes.io/docs/tasks/configmap-secret/managing-secret-using-config-file/)
1. [ ] 💻 [Inject Data into Applications](https://kubernetes.io/docs/tasks/inject-data-application/)
1. [ ] 💻 [Running Stateless Applications](https://kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment/)

After you have completed the guided trainings above, you should be able to answer the following questions and hands-on tasks:

1. [ ] What component runs the API Server of your cluster? Control-Plane or Worker Nodes?
1. [ ] What kind of Kubernetes resource should be used to deploy a stateless application?
1. [ ] What kind of Kubernetes resource should be used to deploy a stateful application?
1. [ ] What kind of Kubernetes resource should be used to when you want to run an application on **every** or a **subset** of worker node(s)
